<?php

function uc_shopauskunft_adminsettings() {
  $form = array();

  $form['services'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shopauskunft.de Settings'),
  );
  
  $days = array("" => "Bitte wählen",
      "activ" => "Aktiv",
      "development" => "Test",
      );
  $form['services']['uc_shopauskunft_settings_mode'] = array(
    '#type' => 'select',
    '#title' => t('Modus'),
    '#default_value' => variable_get('uc_shopauskunft_settings_mode', ''),
    '#options' => $days,
    '#required' => TRUE,
    '#suffix' => t('Im Testmodus werden alle verschickten Emails, an Ihre Testmailadresse gesendet. Sie können somit die Funktion bequem testen. '),
  );
  
  $form['services']['uc_shopauskunft_settings_development_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Testmailadresse'),
    '#required' => TRUE,
    '#default_value' => variable_get('uc_shopauskunft_settings_development_email', ''),
  );
  
  $form['services']['uc_shopauskunft_settings_shopauskunft_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Shopauskunft ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('uc_shopauskunft_settings_shopauskunft_id', ''),
  );
  
  $days = array("" => "Bitte wählen", 0 => 0,1 => 1,2 => 2,3 => 3,4 => 4,5 => 5,6 => 6,7 => 7,8 => 8,9 => 9,10 => 10,
      11 => 11,12 => 12,13 => 13,14 => 14,15 => 15,16 => 16,17 => 17,18 => 18,19 => 19,
      20 => 20,21 => 21,22 => 22,23 => 23,24 => 24);
  $form['services']['uc_shopauskunft_settings_email_days'] = array(
    '#type' => 'select',
    '#title' => t('Versand der Bewertungsemail xxx Tage nach dem Versand'),
    '#default_value' => variable_get('uc_shopauskunft_settings_email_days', ''),
    '#options' => $days,
    '#required' => TRUE,
    '#suffix' => t('0 = Emailversand erfolgt sofort nach Versand'),
  );
  
  $form['services']['uc_shopauskunft_settings_email_time'] = array(
    '#type' => 'select',
    '#title' => t('Uhrzeit'),
    '#default_value' => variable_get('uc_shopauskunft_settings_email_time', ''),
    '#options' => $days,
    '#required' => TRUE,
    '#suffix' => t('Legt die Uhrzeit fest zu der die Bewertungsemail versand wird. 0 = keine Einschränkung'),
  );
  
  $form['services']['uc_shopauskunft_settings_max_days'] = array(
    '#type' => 'select',
    '#title' => t('Versende keine Email wenn zwischen Bestellung und Auslieferung mehr als xxx Tage liegen'),
    '#default_value' => variable_get('uc_shopauskunft_settings_max_days', ''),
    '#options' => $days,
    '#required' => TRUE,
    '#suffix' => t('0 = Ignoriere diese Einstellung'),
  );
  
  $form['services']['uc_shopauskunft_settings_subject_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Subjekt'),
    '#required' => TRUE,
    '#default_value' => variable_get('uc_shopauskunft_settings_subject_email', '[uc_store_name] - Bewertungsemail'),
    '#suffix' => t('<b>Mögliche Variablen:  ([uc_store_name])</b><br>'),
  );
  
  $form['services']['uc_shopauskunft_settings_shopauskunft_email'] = array(
    '#type' => 'textarea',
    '#title' => t('Bewertungsemail Shopauskunft'),
    '#required' => TRUE,
    '#default_value' => variable_get('uc_shopauskunft_settings_shopauskunft_email', 'Sehr geehrte/r [first_name] [last_name],<br>vielen Dank für Ihre Bestellung vom [order_date] in unserem Shop [uc_store_name].<br><br>
        Die Meinung unserer Kunden ist uns sehr wichtig, deshalb bitten wir Sie unseren Shop zu bewerten. Ihre Bewertung könenn sie bequem unter <a href="[shopauskunft_ratinglink]">Shopauskunft.de</a> abgeben. <br><br>
        Wir ebdanken uns schon im Voraus für Ihr Feedback<br><br>
        mit freundlichen Grüssen<br><br>
        Ihr [uc_store_name] - Team'),
    '#suffix' => t('<b>Possible variables:  ([order_id], [first_name], [last_name], [customer_email], [order_total], [shopauskunft_ratinglink], [delivery_date], [order_date])</b><br>'),
  );

  $form['services']['uc_shopauskunft_settings_mail_footer'] = array(
    '#type' => 'textarea',
    '#title' => t('Fußzeile für Mails'),
    '#default_value' => variable_get('uc_shopauskunft_settings_mail_footer', ''),
  );
  
  $form['services']['uc_shopauskunft_settings_profil_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link zu Ihrem Shopauskunft.de Profil'),
    '#default_value' => variable_get('uc_shopauskunft_settings_profil_link', ''),
    '#required' => TRUE,
  );
  
  $form['services']['uc_shopauskunft_settings_rating_widget'] = array(
    '#type' => 'textarea',
    '#title' => t('Shopauskunft.de Rating Widget'),
    '#default_value' => variable_get('uc_shopauskunft_settings_rating_widget', ''),
    '#suffix' => "Kopieren Sie hier den Quellcode Ihres Shopauskunft.de Rating Widgets. Das Widget 
        kann über die Blockeinstellungen Ihrer Seite hinzugefügt werden."
  );
  
  $form['#validate'] = array('uc_shopauskunft_adminsettings_validate');
  return system_settings_form($form);
}

function uc_shopauskunft_adminsettings_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['uc_shopauskunft_settings_shopauskunft_id'])) {
    form_set_error('', t('The Shopauskunft_id must be numeric'));
  }
  if (is_numeric($form_state['values']['uc_shopauskunft_settings_email_time'])) {
      $cron_time = uc_shopauskunft_crontime($form_state['values']['uc_shopauskunft_settings_email_time']);
      variable_set('uc_shopauskunft_settings_cron_time', $cron_time);
  }
  
  $uc_shopauskunft_settings_mode_old = variable_get('uc_shopauskunft_settings_mode_old', '');
  if($form_state['values']['uc_shopauskunft_settings_mode'] == "active" && !$uc_shopauskunft_settings_mode_old == "active") {
      uc_shopauskunft_exclude_customers();
  } else if ($form_state['values']['uc_shopauskunft_settings_mode'] == 'development') {
      /**
       * this forces execution on next time running cron
       */
      variable_set('uc_shopauskunft_settings_cron_time', 2);
  }
}
