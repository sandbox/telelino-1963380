uc_shopauskunft your leading module for iputting shopauskunft.de services to your Ubercart installation.

Installing:
1. First go to yourdomain.com/admin/build/modules and activate uc_shopasukunft module
2. All neccesarry datatables will be isntalled to your database.
     Notice, on first time installing uc_shopauskunft all orders older than 14 days will be excluded from sending 
     rating request emails. If you like to send this customers rating request emails, too, please change the settings 
     in uc_shopauskunft_install().
3. Move to yourdomain.com/admin/user/permissions and set access permissions for uc_shopauskunft
4. Configure your personal uc_shopauskunft module settings under yourdomain.com/admin/store/shopauskunft/settings/
5. Chose one ore more of the provided uc_shopauskunft blocks to add the different ratting widgets of shopauskunft.de to your store
6. All your users will now receive automaticaly personal rating request mails

We hope uc_shopauskunft helps you to increase the success of your online business. 
